from django.shortcuts import render, redirect
from django.contrib.auth import logout
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from django.views.decorators.csrf import csrf_exempt
from django.http.response import HttpResponse
from social_django.models import UserSocialAuth
from github import Github
from main import sensitive_data
from json import loads


def get_github(user):
    # Todo: may be not found
    user_social_auth = UserSocialAuth.objects.get(user=user)
    access_token = user_social_auth.extra_data['access_token']
    return Github(user_social_auth.user.username, access_token)


def get_auth_github(username):
    user = User.objects.get(username=username)
    return get_github(user)


def index(request):
    return render(request, 'index.html')


@login_required
def repos(request):
    g = get_github(request.user)
    repos = g.get_user().get_repos()

    repo_data = []
    for repo in repos:
        repo_data.append({'name': repo.name, 'full_name': repo.full_name})

    return render(request, 'repos.html', {'repos': repo_data})


@login_required
@require_http_methods(['POST'])
def create_hook(request, repo_name):
    
    g = get_github(request.user)
    user = g.get_user()
    repo = user.get_repo(repo_name)

    print(repo)

    config = {
        'url': sensitive_data.WEBHOOK_URL,
        'content_type': 'json',
    }
    try:
        hook = repo.create_hook('web', config, ['pull_request'], True)
        print(hook)
    except:
        # Do nothing
        pass

    return redirect('/repos/')


@csrf_exempt
@require_http_methods(['POST'])
def handle_hook(request):

    print(request.body)

    body = loads(request.body.decode())

    if not 'action' in body or body['action'] != 'opened':
        return HttpResponse()

    print('\nOPENED\n')

    repo_name = body['repository']['name']
    owner = body['repository']['owner']['login']

    print(owner)

    g = get_auth_github(owner)
    user = g.get_user()
    repo = user.get_repo(repo_name)

    print(repo)

    pull = repo.get_pulls()[0]

    print(pull)

    file = pull.get_files()[0]

    print(file)

    print(file.filename, file.contents_url, file.blob_url)

    commit = pull.get_commits()[0]

    print(commit)

    pull.create_comment('service', commit, 'file', 1)

    return HttpResponse()


@login_required
def logout_user(request):
    logout(request)
    return redirect('/')
