#!/bin/bash

python manage.py makemigrations
python manage.py migrate
gunicorn gcon.wsgi  --workers 3 \
  --bind=unix:./run/gunicorn.sock
