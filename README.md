# gcon

Currently under development

### Getting started

1. Create sensitive_data.py file in gcon/gcon directory with GitHub auth application data:
```
CLIENT_ID = 'ID_HERE'
CLIENT_SECRET = 'SECRET_HERE'
```

2. Create sensitive_data.py file in gcon/main directory with GitHub auth application data and webhook url:
```
WEBHOOK_URL = 'URL_HERE'
CLIENT_ID = 'ID_HERE'
CLIENT_SECRET = 'SECRET_HERE'
```

3. Run app with docker-compose:
```
docker-compose build
docker-compose up -d
```

4. Open url [http://127.0.0.1](http://127.0.0.1)

5. Done